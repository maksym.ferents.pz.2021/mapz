﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int PetAmount { get; set; }
        public override string ToString()
        {
            return $"Person:\n\tName: {Name},\n\tAge: {Age},\n\tpets: {PetAmount}";
        }
    }
}
