﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    abstract class Person
    {
        public string Name;
    }

    interface IShootable
    {
        public void Shoot();
    }

    class Recruit : Person, IShootable
    {
        public Recruit(string name)
        {
            Name = name;
        }

        public void Shoot()
        {
            Console.WriteLine("Shoot!");
        }
    }
}
