﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    internal class Third
    {
        PrivateClass privateClass;
        ProtectedClass protectedClass;
        private class PrivateClass
        {
        }

        protected class ProtectedClass
        {
        }
    }
    
    class Third1 : Third
    {
        //PrivateClass privateClass1;
        ProtectedClass protectedClass1;
        Third1()
        {
            
        }
    }
}
