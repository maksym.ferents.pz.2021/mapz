﻿namespace SpacePirates.Classes
{
    public class Spaceship
    {
        public int BlasterLvl { get; set; } = 0;
        public int SpaceshipLvl { get; set; } = 0;
        public int CrewLvl { get; set; } = 0;

        private Spaceship()
        {

        }
        private static Spaceship instance;
        public static Spaceship GetInstance()
        {
            if (instance == null)
                instance = new Spaceship();
            return instance;
        }
    }
}
