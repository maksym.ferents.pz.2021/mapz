﻿using SpacePirates.Classes;
using System.Windows;

namespace SpacePirates
{
    public partial class MainWindow : Window
    {
        Stronghold citadel;
        Spaceship spaceship;
        public MainWindow()
        {
            InitializeComponent();

            citadel = Stronghold.GetInstance();
            Stronghold citadel1 = Stronghold.GetInstance();
            spaceship = Spaceship.GetInstance();

            if (citadel == citadel1)
                MessageBox.Show("Singleton is working!", "Success");
        }

        private void UpgradeBlaster_Click(object sender, RoutedEventArgs e)
        {
            citadel.UpgradeBlaster(spaceship);
            tb_blaster.Text = spaceship.BlasterLvl.ToString();
        }
        private void UpgradeSpaceship_Click(object sender, RoutedEventArgs e)
        {
            citadel.UpgradeSpaceship(spaceship);
            tb_spaceship.Text = spaceship.SpaceshipLvl.ToString();

        }
        private void UpgradeCrew_Click(object sender, RoutedEventArgs e)
        {
            citadel.UpgradeCrew(spaceship);
            tb_crew.Text = spaceship.CrewLvl.ToString();

        }
        private void Pandora_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            Planet pandora = new Planet(new Pandora());
            tb_popup.Text = pandora.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Jupiter_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            Planet jupiter = new Planet(new Jupiter());
            tb_popup.Text = jupiter.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Mars_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            Planet mars = new Planet(new Mars());
            tb_popup.Text = mars.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Sirius_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            Planet sirius = new Planet(new Sirius());
            tb_popup.Text = sirius.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Saturn_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            Planet saturn = new Planet(new Saturn());
            tb_popup.Text = saturn.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Stronghold_Click(object sender, RoutedEventArgs e)
        {
            sp_upgrades.Visibility = Visibility.Visible;
            tb_title.Visibility = Visibility.Visible;
        }
        private void HideMarket()
        {
            sp_upgrades.Visibility = Visibility.Hidden;
            tb_title.Visibility = Visibility.Hidden;
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
        }
    }
}
