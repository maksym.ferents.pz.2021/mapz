﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    public class Person_Compare : IComparer<Person>
    {
        public int Compare(Person person1, Person person2)
        {
            if (person1.Age > person2.Age)
                return 1;
            else if (person1.Age < person2.Age)
                return -1;
            return 0;
        }

    }
}
