﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    internal class Tenth
    {
        private float a;

        public Tenth(float x)
        {
            this.a = x;
        }
        
        public static implicit operator float(Tenth Tenth) => Tenth.a;
        public static explicit operator Tenth(float a) => new Tenth(a);

        //Tenth tenth = new Tenth(10);

        //float number = tenth; //implicit
        //Tenth newTenth = (Tenth)number; //explicit
    }
    
}
