using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Program;

namespace Lab2_Tests
{
    [TestClass]
    public class MyFuncsTests
    {
        Person Oleh = new Person { Name = "Oleh", Age = 30, PetAmount = 6 };
        Person Julia = new Person { Name = "Julia", Age = 16, PetAmount = 2 };
        Person Petro = new Person { Name = "Petro", Age = 21, PetAmount = 3 };
        Person Anastasia = new Person { Name = "Anastasia", Age = 18, PetAmount = 0 };
        Person Pavlo = new Person { Name = "Pavlo", Age = 5, PetAmount = 1 };
        Person Olexandr = new Person { Name = "Olexandr", Age = 24, PetAmount = 2 };

        [TestMethod]
        public void MySelectTest()
        {
            List<Person> list = new List<Person>() { Oleh, Julia, Petro, Anastasia, Pavlo, Olexandr };
            int[] expectedList = new int[] {
                Oleh.PetAmount, Julia.PetAmount, Petro.PetAmount,
                Anastasia.PetAmount, Pavlo.PetAmount, Olexandr.PetAmount
            };

            CollectionAssert.AreEqual(expectedList, Test.mySelect(list));
        }

        [TestMethod]
        public void MyWhereTest()
        {
            List<Person> list = new List<Person>() { Oleh, Julia, Petro, Anastasia, Pavlo, Olexandr };
            List<Person> expectedList = new List<Person>() { Julia, Anastasia, Pavlo };
            
            CollectionAssert.AreEqual(expectedList, Test.MyWhere(list));
        }

        [TestMethod]
        public void MyListDictionaryTest()
        {
            var listDictionary = new Dictionary<Person, Gender>() {
                { Oleh, Gender.Male },
                { Julia, Gender.Female },
                { Petro, Gender.Male },
                { Anastasia, Gender.Female },
                { Pavlo, Gender.Male },
                { Olexandr, Gender.Male }
            };
            string[] expectedNames = new List<string> { "Oleh", "Petro", "Pavlo", "Olexandr" }.ToArray();

            CollectionAssert.AreEqual(expectedNames, Test.MyListDictionary(listDictionary));
        }

        [TestMethod]
        public void MyExtentionsTest()
        {
            Person person = new Person { Name = "Somebody", PetAmount = 1 };
            person.Age = person.GenerateAge();

            var Anonymous = new
            {
                Name = "Anonymous",
                Country = "USA",
                Age = 30
            };
            Assert.AreEqual(Anonymous.Age, person.Age);
        }

        [TestMethod]
        public void MyComparerTest()
        {
            var people = new Person[] { Oleh, Julia, Petro, Anastasia, Pavlo, Olexandr };
            var expectedArr = new Person[] { Pavlo, Julia, Anastasia, Petro, Olexandr, Oleh };

            CollectionAssert.AreEqual(expectedArr, Test.MyComparer(people));
        }

        [TestMethod]
        public void MySortsTest()
        {
            var people = new List<Person> { Oleh, Julia, Petro, Anastasia, Pavlo, Olexandr };
            var expectedPeople = new List<Person> { Oleh, Petro, Julia, Olexandr, Pavlo, Anastasia };

            CollectionAssert.AreEqual(expectedPeople, Test.MySorts(people));
        }

        [TestMethod]
        public void MySortedListsTest()
        {
            var slist = new SortedList<int, Person> {
                {5, new Person { Name = "Oleh", Age = 30, PetAmount = 6 } },
                {4, new Person { Name = "Julia", Age = 16, PetAmount = 2 } },
                {0, new Person { Name = "Petro", Age = 21, PetAmount = 3 } },
                {3, new Person { Name = "Anastasia", Age = 18, PetAmount = 0 } },
                {1, new Person { Name = "Pavlo", Age = 5, PetAmount = 1 } },
                {2, new Person { Name = "Olexandr", Age = 24, PetAmount = 2 } }
            };
            string expectedRes =
                "5 - Oleh\n" +
                "4 - Julia\n" +
                "3 - Anastasia\n" +
                "2 - Olexandr\n" +
                "1 - Pavlo\n" +
                "0 - Petro\n";

            Assert.AreEqual(expectedRes, Test.MySortedLists(slist));
        }
    }

}