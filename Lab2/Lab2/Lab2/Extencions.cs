﻿using Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    public static class Extencions
    {
        public static string GeneratePet(this Person person)
        {
            return new List<string>() { "Horse", "Dog", "Cat", "Mouse" }[new Random().Next(0, 4)];
        }
        public static int GenerateAge(this Person person)
        {
            return 30;
        }
        public static Gender GenerateGender(this Person person)
        {
            return (Gender)new Random().Next(0, 2);
        }

    }
}
