#include <iostream>
#include <chrono>
using namespace std;

class Test {
	int a, b, c;
public:
	void Add() {
		a = 100 + 100;
	}
	void Div() {
		b = 150 - 100;
	}
	void Mul(){
		c = 100 * 15; 
	}
};

int main() {
	
	auto start = chrono::steady_clock::now();
	for (int i = 0; i < 10000000; i++) {
		Test test;
		test.Add();
		test.Div();
		test.Mul();
	}
	auto end = chrono::steady_clock::now();
	auto diff = end - start;
	cout << "Time: " << chrono::duration <double, milli>(diff).count() << "ms" << endl;
}