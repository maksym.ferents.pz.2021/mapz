﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Passanger
    {
        public int NumberOfTicket { get; set; }
        public string Name { get; set; }

        public Passanger(int NumberOfTicket, string Name)
        {
            this.NumberOfTicket = NumberOfTicket;
            this.Name = Name;
        }

        public Passanger()
        {
            this.NumberOfTicket = 666;
            this.Name = "Mykhail Bulhakov";
        }
    }

    class PremiumClassPassenger : Passanger
    {
        public string Premium { get; set; }

        public PremiumClassPassenger(int NumberOfTicket, string Name, string Premium) : 
            base(NumberOfTicket, Name)
        {
            this.Premium = Premium;
        }
    }
}
