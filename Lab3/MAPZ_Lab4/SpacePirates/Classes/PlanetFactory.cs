﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SpacePirates.Classes
{
    class Planet
    {
        public string Name { get; set; }
        public Disaster Disaster { get; set; }
        public int Chests { get; set; }
        public List<Unit> Pirates { get; set; }
        public Planet(PlanetFactory planetFactory)
        {
            this.Name = planetFactory.GetName();
            this.Pirates = planetFactory.SetPirates();
            this.Disaster = planetFactory.CreateDisaster();
            this.Chests = planetFactory.SetChests();
        }
        public string GetInfo()
        {
            return $"{Name}\n" +
                $"Disaster: {Disaster}\n" +
                $"Chests: {Chests}\n" +
                $"Cannon pirates: {Pirates.Where(x => x.GetType() == typeof(CannonPirate)).Count()} {Pirates.Where(x => x.GetType() == typeof(CannonPirate)).First().Attack()}\n" +
                $"Sword pirates: {Pirates.Where(x => x.GetType() == typeof(SwordPirate)).Count()} {Pirates.Where(x => x.GetType() == typeof(SwordPirate)).First().Attack()}";
        }
    }
    enum Disaster
    {
        Blizzard,
        ToxicRain,
        VolcanoEruption,
        Tsunami,
        Earthquake
    }
    interface PlanetFactory
    {
        Disaster CreateDisaster();
        int SetChests();
        List<Unit> SetPirates();
        string GetName();
    }
    class Pandora : PlanetFactory
    {
        public Disaster CreateDisaster()
        {
            return Disaster.Blizzard;
        }

        public string GetName()
        {
            return "Pandora";
        }

        public int SetChests()
        {
            return 10;
        }
        public List<Unit> SetPirates()
        {
            List<Unit> pirates = new List<Unit>();
            CannonPirate cp = new CannonPirate(3, 10);
            SwordPirate sp = new SwordPirate(10, 3);
            Random rnd = new Random();
            int number = rnd.Next(5, 10);
            for (int i = 0; i < number; i++)
            {
                if (number % (i+1) == 0)
                    pirates.Add(cp.Clone());
                pirates.Add(sp.Clone());
            }
            return pirates;
        }
    }
    class Mars : PlanetFactory
    {
        public Disaster CreateDisaster()
        {
            return Disaster.Earthquake;
        }

        public string GetName()
        {
            return "Mars";
        }

        public int SetChests()
        {
            return 13;
        }
        public List<Unit> SetPirates()
        {
            List<Unit> pirates = new List<Unit>();
            CannonPirate cp = new CannonPirate(6, 13);
            SwordPirate sp = new SwordPirate(13, 6);
            Random rnd = new Random();
            int number = rnd.Next(6, 11);
            for (int i = 0; i < number; i++)
            {
                if (number % (i+1) == 0)
                    pirates.Add(cp.Clone());
                pirates.Add(sp.Clone());
            }
            return pirates;
        }
    }
    class Jupiter : PlanetFactory
    {
        public Disaster CreateDisaster()
        {
            return Disaster.ToxicRain;
        }

        public string GetName()
        {
            return "Jupiter";
        }

        public int SetChests()
        {
            return 6;
        }
        public List<Unit> SetPirates()
        {
            List<Unit> pirates = new List<Unit>();
            CannonPirate cp = new CannonPirate(1, 6);
            SwordPirate sp = new SwordPirate(6, 1);
            Random rnd = new Random();
            int number = rnd.Next(3, 7);
            for (int i = 0; i < number; i++)
            {
                if (number % (i+1) == 0)
                    pirates.Add(cp.Clone());
                pirates.Add(sp.Clone());
            }
            return pirates;
        }
    }
    class Sirius : PlanetFactory
    {
        public Disaster CreateDisaster()
        {
            return Disaster.Tsunami;
        }

        public string GetName()
        {
            return "Sirius";
        }

        public int SetChests()
        {
            return 19;
        }
        public List<Unit> SetPirates()
        {
            List<Unit> pirates = new List<Unit>();
            CannonPirate cp = new CannonPirate(12, 19);
            SwordPirate sp = new SwordPirate(19, 12);
            Random rnd = new Random();
            int number = rnd.Next(10, 20);
            for (int i = 0; i < number; i++)
            {
                if (number % (i+1) == 0)
                    pirates.Add(cp.Clone());
                pirates.Add(sp.Clone());
            }
            return pirates;
        }
    }
    class Saturn : PlanetFactory
    {
        public Disaster CreateDisaster()
        {
            return Disaster.VolcanoEruption;
        }

        public string GetName()
        {
            return "Saturn";
        }

        public int SetChests()
        {
            return 23;
        }
        public List<Unit> SetPirates()
        {
            List<Unit> pirates = new List<Unit>();
            CannonPirate cp = new CannonPirate(16, 23);
            SwordPirate sp = new SwordPirate(23, 16);
            Random rnd = new Random();
            int number = rnd.Next(15, 30);
            for (int i = 0; i < number; i++)
            {
                if (number % (i+1) == 0)
                    pirates.Add(cp.Clone());
                pirates.Add(sp.Clone());
            }
            return pirates;
        }
    }
}
