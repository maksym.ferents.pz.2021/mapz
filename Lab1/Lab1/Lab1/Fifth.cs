﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    interface ICat
    {
        void Meow(int martsin);
    }
    interface IDog
    {
        void Wolf(String terendii);
    }
    interface ICatDog
    {
        void Speak();
    }
    class Cat : ICat
    {
        public void Meow(int martsin)
        {
            Console.WriteLine("A ya vam kazala hodyty na lektsii! @{0}", martsin);
        }
    }

    class Dog : IDog
    {
        public void Wolf(String terendii)
        {
            Console.WriteLine("A ya vam kazala pysaty konspekty! @{0}", terendii);
        }
    }
    class CatDog : ICatDog
    {
        public void Speak()
        {
            Console.WriteLine("A ya vam kazala sluhaty pani Levus i dotrymuvatys' AKADEMICHNOYI DOBROCHESNOSTI!!!");
        }
    }
    class TK : ICat, IDog, ICatDog
    {
        Cat danya = new Cat();
        Dog tolik = new Dog();
        CatDog sashko = new CatDog();

        public void Meow(int martsin)
        {
            danya.Meow(martsin);
        }
        public void Wolf(String terendii)
        {
            tolik.Wolf(terendii);
        }
        public void Speak()
        {
            sashko.Speak();
        }
    }
}
