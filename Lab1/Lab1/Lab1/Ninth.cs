﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    internal class Ninth
    {
        public void Number()
        {
            int num = 10;
            object numObj = num; //boxing
            Console.WriteLine(numObj); //unboxing

            int num1 = (int)numObj; //unboxing #2
            Console.WriteLine(num1);
        }
    }
}
