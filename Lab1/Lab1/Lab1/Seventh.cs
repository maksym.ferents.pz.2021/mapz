﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Human
    {
        public static int CountOfTeeth = 32;
        public double weight;

        static Human()
        {
            CountOfTeeth = 28;
            Console.WriteLine("Static constructor");
        }

        public Human()
        {
            CountOfTeeth--;
            weight = 70;
            Console.WriteLine("Dynamic Constructor");
        }
        
        Human dima = new Human()
        {
            weight = 35
        };
    }
}
