﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    internal class House
    {
        public int CountOfMembers { get; private set; }
        public double Square { get; private set; }
        public string Adress { get; private set; }

        public House(int countOfMembers, double square, string adress)
        {
            CountOfMembers = countOfMembers;
            Square = square;
            Adress = adress;
        }

        public override bool Equals(object otherObject)
        {

            if ((otherObject is House otherHouse) == false)
                return false;

            House house = (House)otherObject;
            return (CountOfMembers == house.CountOfMembers) && (Square == house.Square) && (Adress == house.Adress);
        }

        public static new bool Equals(object first, object second)
        {
            return first.Equals(second);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"{CountOfMembers}, {Square}, {Adress}";
        }

        public new Type GetType()
        {
            return base.GetType();
        }
        public static new bool ReferenceEquals(object objA, object objB)
        {
            return objA == objB;
        }

        protected new object MemberwiseClone()
        {
            return base.MemberwiseClone();
        }

        /*static void Main(string[] args)
        {
            var houseFirst = new House(9, 350.0, "Amogus");
            var houseSecond = new House(9, 350.0, "Amogus");
            var houseThird = houseFirst;

            Console.WriteLine("1&2 objects are equal:" + Object.ReferenceEquals(houseFirst, houseSecond));
            Console.WriteLine("1&2 objects have same values:" + Object.Equals(houseFirst, houseSecond));
            Console.WriteLine("1&3 objects are equal:" + Object.ReferenceEquals(houseFirst, houseThird + "\n\n"));

            Console.WriteLine("House's value is: " + houseFirst.ToString());
            Console.WriteLine("Object type: " + houseFirst.GetType());
            Console.WriteLine("Object hashcode: " + houseFirst.GetHashCode());
        }*/
    }
}
