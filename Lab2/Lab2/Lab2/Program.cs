﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Program
{
    public enum Gender { Male, Female }
    public static class Test
    {
        
        public static int[] mySelect(List<Person> list)
        {
            if(list.Any() == false)
            {
                return null;
            }
            else
            {
                return list.Select(person => person.PetAmount).ToArray();
            }
        }

        public static List<Person> MyWhere(List<Person> list)
        {
            if (list.Any() == false)
                return null;
            return list
                .Where(person => person.Age <= 20)
                .ToList();
        }

        public static string[] MyListDictionary(Dictionary<Person, Gender> listDictionary)
        {
            if (listDictionary.Any() == false)
                return null;
            return listDictionary
                .Where(x => x.Value == Gender.Male)
                .Select(person => person.Key.Name)
                .ToArray();
        }

        public static Person[] MyComparer(Person[] people)
        {
            if (people.Any() == false)
                return null;
            Array.Sort(people, new Person_Compare());
            foreach (Person person in people)
                Console.WriteLine($"{person} ");
            return people;
        }

        public static List<Person> MySorts(List<Person> list)
        {
            list.ForEach(person => Console.WriteLine(person));

            Console.WriteLine("\nSort (by age): ");
            list.Sort(new Person_Compare());
            list.ForEach(person => Console.WriteLine(person));

            Console.WriteLine("\nOrderByDescending (by pet amount): ");
            list = list.OrderByDescending(person => person.PetAmount).ToList();
            list.ForEach(person => Console.WriteLine(person));

            return list;
        }

        public static string MySortedLists(SortedList<int, Person> slist)
        {
            var reversedList = slist.Reverse();
            string output = "";
            foreach (var person in reversedList)
                output += String.Format("{0} - {1}\n", person.Key, person.Value.Name);
            return output;
        }

        public static void Main(string[] args)
        {
            Person Oleh = new Person { Name = "Oleh", Age = 30, PetAmount = 6 };
            Person Julia = new Person { Name = "Julia", Age = 16, PetAmount = 2 };
            Person Petro = new Person { Name = "Petro", Age = 21, PetAmount = 3 };
            Person Anastasia = new Person { Name = "Anastasia", Age = 18, PetAmount = 0 };
            Person Pavlo = new Person { Name = "Pavlo", Age = 5, PetAmount = 1 };
            Person Olexandr = new Person { Name = "Olexandr", Age = 24, PetAmount = 2 };

            Pets horse = new Pets { OwnerName = "Petro", Animal = "horse" };
            Pets horse1 = new Pets { OwnerName = "Volodymyr", Animal = "horse" };
            Pets horse2 = new Pets { OwnerName = "Volodymyr", Animal = "horse" };
            Pets mouse = new Pets { OwnerName = "PetroOlex", Animal = "mouse" };
            Pets dog = new Pets { OwnerName = "Oleh", Animal = "dog" };
            Pets cat = new Pets { OwnerName = "Olexandr", Animal = "cat" };
            Pets hamster = new Pets { OwnerName = "Petro", Animal = "hamster" };
            Pets Hamster = new Pets { OwnerName = "Hamserhamser", Animal = "hamster" };
            Pets turtle = new Pets { OwnerName = "Anastasia", Animal = "turtle" };

            var pets = new List<Pets> { horse, dog, cat, hamster, turtle, horse1, mouse, Hamster, horse2 };
            var list = new List<Person> { Oleh, Julia, Petro, Anastasia, Oleh, Pavlo, Olexandr };
            var list2 = new List<Person> { Oleh, Julia, Petro, Anastasia };

            Person person = new Person();
            person.GeneratePet();

            var l = pets.GroupBy(x => x.Animal);

            /*var li = list.TakeWhile(x => x.Age > 20);

            var el = new List<int> {5 }.FirstOrDefault(x => x ==3);

            var chars = Enumerable.Range('a', 'z' - 'a' + 1).Select(i => ((char)i).ToString()).ToList();
            
            for(int i = 1; i <= chars.Count; ++i)
            {
                Console.WriteLine(chars.Take(i).Aggregate((x, y) => x + y));
            }
            var res = list.ToLookup(x => x.PetAmount);
            foreach (var item in res)
            {
                Console.WriteLine($"{item.Key} :");
                foreach (var per in item)
                {
                    Console.WriteLine($"{per}\n");
                }
            }
            list.All(x => x.PetAmount > 3);
            var groupJoin = list.GroupJoin(
                pets,
                person => person.Name,
                pet => pet.OwnerName,
                (person, pet) => new { Person = person, Pet = pet });

            foreach (var item in groupJoin)
            {
                Console.WriteLine($"{item.Person.Name} :");
                foreach (var per in item.Pet)
                {
                    Console.WriteLine($"{per.Animal}");
                }
                Console.WriteLine();
            }
            list.OrderBy(x => x.PetAmount).ToList().ForEach(x => Console.WriteLine(x));
            list.ToLookup(x => x.PetAmount);
            list.Sort(new Person_Compare());
            list.ForEach(x => Console.WriteLine(x));
            var res = from per in list
                      where per.Age > 15
                      select per;
        */
        }
    }
}
