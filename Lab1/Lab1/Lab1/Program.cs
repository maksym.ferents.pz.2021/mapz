﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    /*    class TeethCountTest
        {
            public static void TeethCountTestRef(ref int teethCount)
            {
                teethCount = 32;
                teethCount--;
            }
            public static void TeethCountTestOut(out int teethCount)
            {
                teethCount = 28;
                teethCount--;
            }
        }

        class MainProgram
        {
            static void Main(string[] args)
            {
                int teethCount = 15;
                TeethCountTest.TeethCountTestRef(ref teethCount);
                Console.WriteLine("Teeth count by REF = " + teethCount);
                TeethCountTest.TeethCountTestOut(out teethCount);
                Console.WriteLine("Teeth count by OUT = " + teethCount);
            }
        }*/

    class Program
    {

        /*int crew;
        public double calibr;

        public Program(double caliber, int crew)
        {
            this.calibr = caliber;
            this.crew = crew;
        }
        public void GetCaliber(ref double cal)
        {
            cal = calibr;
        }

        public void GetCrew(out int cr)
        {
            cr = crew;
        }

        public void ChangeRecruit(Recruit recruit)
        {
            recruit.Name = "New";
        }

        static void Main(string[] args)
        {
            Program chmonya = new Program(100, 30);
            double n = 20;
            int b = 0;
            chmonya.GetCaliber(n);
            chmonya.GetCrew(b);

            Console.WriteLine("n = " + n);
            Console.WriteLine("b = " + b);
            

            Recruit recruit = new Recruit("Old");
            Console.WriteLine(recruit.Name);
            chmonya.ChangeRecruit(recruit);
            Console.WriteLine(recruit.Name);
        }*/

        /*int a, b, c;
        public void Add()
        {
            a = 100 + 100;
        }
        public void Dif()
        {
            b = 150 - 100;
        }
        public void Mul()
        {
            c = 100 * 15;
        }*/

        internal class House
        {
            public int CountOfMembers { get; private set; }
            public double Square { get; private set; }
            public string Adress { get; private set; }

            public House(int countOfMembers, double square, string adress)
            {
                CountOfMembers = countOfMembers;
                Square = square;
                Adress = adress;
            }

            public override bool Equals(object otherObject)
            {

                if ((otherObject is House otherHouse) == false)
                    return false;

                House house = (House)otherObject;
                return (CountOfMembers == house.CountOfMembers) && (Square == house.Square) && (Adress == house.Adress);
            }

            public static new bool Equals(object first, object second)
            {
                return first.Equals(second);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public override string ToString()
            {
                return $"{CountOfMembers}, {Square}, {Adress}";
            }

            public new Type GetType()
            {
                return base.GetType();
            }
            public static new bool ReferenceEquals(object objA, object objB)
            {
                return objA == objB;
            }

            protected new object MemberwiseClone()
            {
                return base.MemberwiseClone();
            }
    }

        static void Main(string[] args)
        {

            /*int a, b, c;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 10000000; i++)
            {
                var test = new Program();
                test.Add();
                test.Mul();
                test.Dif();
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds + "ms");*/

            var houseFirst = new House(9, 350.0, "Amogus");
            var houseSecond = new House(9, 350.0, "Amogus");
            var houseThird = houseFirst;

            Console.WriteLine("1&2 objects are equal:" + Object.ReferenceEquals(houseFirst, houseSecond));
            Console.WriteLine("1&2 objects have same values:" + Object.Equals(houseFirst, houseSecond));
            Console.WriteLine("1&3 objects are equal:" + Object.ReferenceEquals(houseFirst, houseThird + "\n\n"));

            Console.WriteLine("House's value is: " + houseFirst.ToString());
            Console.WriteLine("Object type: " + houseFirst.GetType());
            Console.WriteLine("Object hashcode: " + houseFirst.GetHashCode());
        }
    }
}

